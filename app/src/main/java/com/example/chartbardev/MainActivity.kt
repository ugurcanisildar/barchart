package com.example.chartbardev

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.chartbardev.databinding.ActivityMainBinding
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    lateinit var barlist: ArrayList<BarEntry>


    private lateinit var barDataSet: BarDataSet
    private lateinit var barData: BarData
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        barlist = ArrayList()
        barlist.add(BarEntry(6f,20f))
        barlist.add(BarEntry(3f,70f))
        barlist.add(BarEntry(2f,50f))
        barlist.add(BarEntry(1f,30f))
        barlist.add(BarEntry(7f,20f))
        barlist.add(BarEntry(4f,5f))
        barlist.add(BarEntry(5f,11f))
        barlist.add(BarEntry(8f,11f))

        barDataSet =BarDataSet(barlist,"day")

        barData= BarData(barDataSet)
        barDataSet.setColors(getColor(R.color.ZipColor))
        binding.barChart.data = barData
        barDataSet.valueTextColor = Color.BLACK
        barDataSet.valueTextSize= 15f
        barData.barWidth= 0.45f
// seçili barın rengini değiştirir
        barDataSet.highLightColor= getColor(R.color.ZipColor2)
        binding.barChart.setTouchEnabled(true)
        binding.barChart.setFitBars(false)
        binding.barChart.setVisibleXRangeMaximum(4f)
        binding.barChart.moveViewToX(6f)
        binding.barChart.animateY(2000)
        binding.barChart.xAxis.isEnabled = false
        binding.barChart.axisLeft.isEnabled = false
        binding.barChart.axisRight.isEnabled = false
        binding.barChart.highlightValue(null)
    }
}